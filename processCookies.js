const fs = require('fs');
const existingCookies = require('./c.json');

const updatedObject = existingCookies.reduce((acc, val) => {
  const domainCookes = acc[val.domain] || {};
  const pathCookies = domainCookes[val.path] || {};

  return {
    ...acc,
    [val.domain]: {
      ...domainCookes,
      [val.path]: {
        ...pathCookies,
        [val.name]: {
          key: val.name,
          value: val.value,
          // expires: val.expirationDate,
          maxAge: Infinity,
          domain : val.domain,
          path : val.path,
          secure : val.secure,
        },
      }
    }
  }
}, {});


fs.writeFile("./cookies.json", JSON.stringify(updatedObject), function(err) {
  if(err) {
      return console.log(err);
  }

  console.log("The file was saved!");
}); 

console.log(updatedObject);