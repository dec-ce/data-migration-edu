var AWS = require('aws-sdk');
var request = require('request');
var csv = require('fast-csv');
var proxy = require('proxy-agent');
const fs = require('fs');
var util = require('util');
const FileCookieStore = require('tough-cookie-filestore');


if (process.argv.length < 4) {
  throw new Error(`$ yarn start "S3_BUCKET" "CSV_PATH"`);
}

const [ nodePath, filePath, S3_BUCKET, CSV_PATH] = process.argv;

//configuring the AWS environment
//AWS.config.loadFromPath('./config.json');
AWS.config.update({
    httpOptions: {
        agent: proxy('http://proxy.det.nsw.edu.au:80')
    },
    accessKeyId: "AKIAJ7NTXV3G635VPOEA",
    secretAccessKey: "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"
});

/* CONFIG OPTIONS */
// const S3_BUCKET = 'edu-datamigration-delta-play-5dec/private-sites';

const j = request.jar(new FileCookieStore('cookies.json'));
request.defaults({ jar: j });

var s3 = new AWS.S3();
get_urls_fromcsv();

const CONCURRENT = 1;
const pageObjects = [ /* { url, bucket, key } */ ];

var logFile = fs.createWriteStream('log.txt');
var logStdout = process.stdout;

log = function () {
  logFile.write(util.format.apply(null, arguments) + '\n');
  logStdout.write(util.format.apply(null, arguments) + '\n');
};
console.error = log;


function get_urls_fromcsv() {
  var stream = fs.createReadStream(CSV_PATH);
  var csvStream = csv()
    .on("data", addUrl)
    .on("end", processUrls);
  stream.pipe(csvStream)
}

function addUrl(data) {
  pageObjects.push({url: data[0], bucket: S3_BUCKET, key: data[0].split("/").slice(3).join("/") });
}

function requestUrlRecursive() {
  if (!pageObjects || !pageObjects.length) { return undefined; }
  const currentPage = pageObjects.shift();
  put_from_url(currentPage.url, currentPage.bucket, currentPage.key + '.json')
    .then(function () {
      log('Uploaded data successfully at - ' + currentPage.key + '');
      requestUrlRecursive();
    })
    .catch(function (err) {
      log('<ERROR>  ===========================================================================================');
      log('Uploaded data failed at - ' + currentPage.key + '');
      log(err);
      log('</ERROR> ===========================================================================================');
      requestUrlRecursive();
    });
}

function processUrls () {
  for (let i = 0; i < CONCURRENT; i++) {
    requestUrlRecursive();
  }
}

function put_from_url(url, bucket, key) {
  return new Promise(function (resolve, reject) {
    request({
      url: url,
      encoding: null,
      // jar: j,
      headers: {
        'Cookie': '',
      }
    }, function (err, res, body) {
      if (err) {
        reject(err, res);
      }

      try {
        const jsonContent = JSON.parse(body.toString());
      } catch (e) {
        return reject("Invalid JSON content");
      }
      
      // console.log(body.toString());

      s3.putObject({
          Bucket: bucket,
          Key: key,
          ContentType: res.headers['content-type'],
          ContentLength: res.headers['content-length'],
          Body: body // buffer
      }, function () {
        resolve();
      });
    });
  });
}



