<div class=uk-width-large-7-10 ng-cloak">
  <form class="gef-search uk-form  gef-search--body" id="searchForm" name="searchForm" aria-hidden="false" aria-labelledby="searchForm" ng-controller="SearchFormCtrl" ng-submit="searchForm.$valid" novalidate="novalidate">
    <h1 class="gef-search__heading">Search results

      <span><span class="link-tag-container"><span class="link-tag"><i class="link-tag-icon" aria-hidden="aria-hidden"></i>STAFF ONLY</span></span></span>
 
    </h1>
    <fieldset>
      <legend class="show-on-sr">Global Search</legend>
      <label for="search"><span class="show-on-sr">Enter your search</span></label>
      <input class="uk-form-large" type="text" id="search" name="q" placeholder="Search" tabindex="0" required="required" ng-model="keyword" maxlength="254"/>
      <input type="hidden" name="site" value="%globals_site_metadata_SITE.SEARCH.collection%"/>
      <input type="hidden" name="access" value="s"/>
      <button type="submit" id="btnSearch14" aria-label="Submit search" tabindex="0" ng-click="newSearchTerm()"><span class="show-on-sr">Search</span><i class="uk-icon-search"></i></button>
    </fieldset>
  </form>
  <aside class="gef-search-results__mobile-refine gef-show-hide gef-show-hide--alt uk-accordion uk-visible-small" data-uk-accordion="{collapse: false, showfirst:false}" role="tablist" ng-controller="ShowResultsCtrl">
    <h3 class="uk-accordion-title" id="searchRefineundefined" aria-expanded="false" role="tab" tabindex="0" aria-controls="mobileRefine"><span>Refine Results<i></i><i class="vertical"></i></span></h3>
    <div class="uk-accordion-content" id="mobileRefine">
      
         <div>
           <form id="refine-search-form2" action="" method="GET">
             <fieldset>
               <legend class="uk-hidden">Refine Results.  Available options are "All NSW Department of Education public content" or "Only %globals_get_search_start^as_asset:asset_name%".  Changing the selection will refresh the search results.
               </legend>
               <div class="uk-margin-bottom">
                 <div class="gef-radio-list__row">
                    <input type="radio" id="AllEd-mobile" name="radioID-mobile" ng-checked="true" value="education.nsw.edu.au" ng-model="setSearchUrl" ng-change="newSearchScope('%globals_asset_assetid^eq:322919:education.nsw.gov.au: %', '%globals_asset_assetid^eq:322919:Inside the department:NSW Department of Education%')">
                     <label for="AllEd-mobile">%globals_asset_assetid^eq:322919:All Inside the department:All NSW Department of Education public content%</label>
                 </div>
                 <div class="gef-radio-list__row">
                    <input type="radio" id="%globals_get_search_start^as_asset:asset_name^underscore%-mobile" name="radioID-mobile" value="%globals_get_search_start^as_asset:asset_url%" ng-model="setSearchUrl" ng-change="newSearchScope('%globals_get_search_start^as_asset:asset_url%', '%globals_get_search_start^as_asset:asset_name%')">
                    <label for="%globals_get_search_start^as_asset:asset_name^underscore%-mobile">Only %globals_get_search_start^as_asset:asset_name%</label>
                  </div>
                </fieldset>
              </div>
            </form>
          </div>
          <hr class="uk-margin-small gef-hr-light">
          

              <div class="gef-search-results__links">
                <h3 class="uk-h4">Looking for something else?</h3>
             
                  <p><a href="./?a=388351">Search all public content</a></p>
             
                  <p><i class="uk-icon-lock" aria-hidden="true"></i><a href="./?a=322919">Search staff only content</a></p>
              
                
              </div>
           
               <p class="gef-back-to"><i class="uk-icon-chevron-left" aria-hidden="true"></i> Back to <a href="%globals_get_search_start^as_asset:asset_url%">%globals_get_search_start^as_asset:asset_name%</a></p>
          </aside>
        </div>
      </div>
    </div>
    <hr class="gef-hr-light uk-margin-bottom uk-margin-small-top">
    <article class="uk-container uk-container-center" ng-cloak>
      <div class="uk-grid">

   
       <div class="uk-width-1-1 uk-width-medium-7-10 uk-push-3-10" ng-controller="ShowResultsCtrl"> 
          <div class="gef-search-results uk-margin-large-top" id="searchResults" role="region">
            <dir-pagination-controls template-url="./?a=388350?r=%globals_asset_metadata_revision:63506%" ng-hide="hasError"></dir-pagination-controls>


            <ol class="gef-search-results__list" ng-show="results.length > 0">
              <li class="gef-search-results__item" dir-paginate="result in results | itemsPerPage : resultsPerPage" total-items="navigation.total_results" current-page="pageNumber">
                <h2 class="uk-h5 gef-search-results__itemheader"><a href="{{result.url}}" ng-bind-html="result.title | sanitize"></a></h2>
                <p ng-bind-html="result.summary | sanitize">&hellip;</p>
                <p class="gef-search-results__url">{{result.url}}</p>
              </li>
            </ol>
          </div>
          <div class="uk-margin-bottom"></div>
          <p ng-hide="results.length &gt; 0 || searchTerm == ''"></p>
          <dir-pagination-controls class="gef-search-pagination" template-url="./?a=388340?r=%globals_asset_metadata_revision:63506%" on-page-change="pageChanged(newPageNumber)" ng-hide="hasError" max-size="7"></dir-pagination-controls>
          <div class="gef-search-results__links gef-search-results__links--boxed">
            <h3 class="uk-h4">Looking for something else?
              
                  <p><a href="./?a=388351">Search all public content</a></p>
              
                  <p><i class="uk-icon-lock" aria-hidden="true"></i><a href="./?a=322919">Search staff only content</a></p>
              
            </h3>
            
          </div>
          <p class="error" ng-show="hasError"> An error has occurred whilst retrieving results. Our technical team has been notified.</p>
        </div>

       <aside class="uk-width-medium-3-10 uk-hidden-small uk-pull-7-10" ng-controller="ShowResultsCtrl">
       
        <h2 class="gef-search-refine-results-header uk-margin-top">Refine Results</h2>
          <fieldset>
            <legend class="uk-hidden">Refine Results.  Available options are "All NSW Department of Education public content" or "Only %globals_get_search_start^as_asset:asset_name%".  Changing the selection will refresh the search results.</legend>
             <div class="uk-margin-bottom">
              <div class="gef-radio-list__row">
              <input type="radio" ng-checked="true" id="AllEd" name="radioID" value="education.nsw.edu.au" ng-model="setSearchUrl" ng-change="newSearchScope('%globals_asset_assetid^eq:322919:education.nsw.gov.au: %', '%globals_asset_assetid^eq:322919:Inside the department:NSW Department of Education%', 'AllEd')">
              <label for="AllEd">%globals_asset_assetid^eq:322919:All Inside the department:All NSW Department of Education public content%</label>
            </div>
            <div class="gef-radio-list__row">
                  <input type="radio" id="%globals_get_search_start^as_asset:asset_name^underscore%" name="radioID" value="%globals_get_search_start^as_asset:asset_url%" ng-model="setSearchUrl" ng-change="newSearchScope('%globals_get_search_start^as_asset:asset_url%', '%globals_get_search_start^as_asset:asset_name%')">
                  <label for="%globals_get_search_start^as_asset:asset_name^underscore%">Only %globals_get_search_start^as_asset:asset_name%</label>
            </div>            
            
            
          </div>
          </fieldset>
         <hr class="gef-hr-light uk-margin">
       
          <div class="gef-search-results__links">
            <h3 class="uk-h4">Looking for something else?</h3>
          
             <p><a href="./?a=388351">Search all public content</a></p>
          
            <p><i class="uk-icon-lock" aria-hidden="true"></i><a href="./?a=322919">Search staff only content</a></p>
   
          </div>
    
           <hr class="gef-hr-light uk-margin uk-margin-large-top">
           <p class="gef-back-to"><i class="uk-icon-chevron-left" aria-hidden="true"></i> Back to <a href="%globals_get_search_start^as_asset:asset_url%">%globals_get_search_start^as_asset:asset_name%</a></p>
    
       </aside>
      </div>  
    </article>     