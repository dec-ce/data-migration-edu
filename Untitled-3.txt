<tr>
  <td headers="table08340r1c1">Wideview Public School</td>
  <td headers="table08340r1c2"> <a href="https://wideview-p.schools.nsw.gov.au">https://wideview-p.schools.nsw.gov.au</a></td>
</tr>
<tr>
  <td headers="table08340r1c1">Meadowbank Public School</td>
  <td headers="table08340r1c2"> <a href="https://meadowbank-p.schools.nsw.gov.au">https://meadowbank-p.schools.nsw.gov.au</a></td>
</tr>
<tr>
  <td headers="table08340r1c1">Turramurra Public School</td>
  <td headers="table08340r1c2"> <a href="https://turramurra-p.schools.nsw.gov.au">https://turramurra-p.schools.nsw.gov.au</a></td>
</tr>
<tr>
  <td headers="table08340r1c1">Cardiff High School</td>
  <td headers="table08340r1c2"> <a href="https://cardiff-h.schools.nsw.gov.au">https://cardiff-h.schools.nsw.gov.au</a></td>
</tr>
